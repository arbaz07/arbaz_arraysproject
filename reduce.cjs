

function reduce(element,callBack,startingValue){
    if(typeof(element)=='object' && Array.isArray(element)){
        let index=0;
        if(typeof(startingValue)=='undefined'){
            startingValue=element[0];
            index=1;
        }
        let result=0;
        for (let elementIndex=index; elementIndex<element.length; elementIndex++){
            result=callBack(startingValue,element[elementIndex], elementIndex, element)
            startingValue=result;
        }
        return result;
    }else{
        return 0;
    }
}




// const objects = [{ x: 1 }, { x: 2 }, { x: 3 }];
// const sum = objects.reduce(
//   (accumulator, currentValue) => accumulator + currentValue.x,
//   0,
// );
// function red(a,b){
//     return a+b.x;
// }

// console.log(sum); // 6

// const getMax = (a, b) => Math.max(a, b);

// //console.log(reduce([1, 100],getMax, 50)); // 100
// console.log(reduce()); // 50

// // callback is invoked once for element at index 1
// [1, 100].reduce(getMax); // 100

// // callback is not invoked
// [50].reduce(getMax); // 50
// [].reduce(getMax, 1); // 1


module.exports = reduce;