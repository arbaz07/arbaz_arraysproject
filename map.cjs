

function map(element, callBack){
    if(Array.isArray(element) && typeof(callBack)=='function' ){
        let newArray = [];
        for (let index=0; index<element.length; index++){
            let result = callBack(element[index],index, element);
            newArray.push(result);
        }
        return newArray;
    }
    return 0;
    
}
// const arr=["12","23j",".343hjh2"];
// console.log(arr.map((num)=> parseInt(num)));


// function cb(ele){

//     return undefined;
// }

// const res=map();


// console.log(res);


module.exports = map;