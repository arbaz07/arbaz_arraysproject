const _ = require('underscore');

// each([1,2,3,4,5,6,10,7,6], cb)

function each(element,callback){
    let eachelement = [];
    console.log(typeof(callback));
    if(element instanceof Array && typeof(callback)=='function'){
        for(index=0; index<element.length; index++){
            eachelement.push(callback(element,index))
        }
        return eachelement;
    }
    return [];
}

module.exports = each;