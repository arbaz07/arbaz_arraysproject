



function flatten(element, depth=1){
    if(Array.isArray(element) && typeof(depth)=='number'){
        let flatArray=[];
        for (let items of element){
            if(items === undefined){
                continue;
            }
            //flatArray = flatArray.concat(items);
            flatArray= flatArray.concat(items);
        }
        depth--;
        if(depth>0){
            return flatten(flatArray, depth);
        }else{
            return flatArray;
        }
    }
    return element;
}






// function flatten(element, depth=1) {

//     if(Array.isArray(element) && typeof(depth)=='number'){
     
//         let flattenArray = [];
//         function innerFlattenFunction(elements, depth){
//             for(let item in elements) {
//                 if(typeof(item)=='undefined'){
//                     continue;
//                 }else if(Array.isArray(item) && depth<=0 ) {
//                     flattenArray.push(item);
//                 }else {
//                     innerFlattenFunction(item,depth-1);
//                 }
//             }
//         }
//         innerFlattenFunction(element, depth);
//         return flattenArray;
        
//     }
//     return [];
// }





const arr1 = [0,, 2, [[[[3, 4]]]]];
const binn={};

console.log(arr1.flat(2,3));

console.log(flatten(arr1,2,3));


// expected output: [0, 1, 2, [3, 4]]


module.exports = flatten;