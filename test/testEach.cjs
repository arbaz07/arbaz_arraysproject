const each = require("../each.cjs")
const data = require("../data.cjs")



function cb(element,i){
    if(typeof(element)!='undefined')
    return element[i];
    return;
}

let result = each(data,cb);
console.log(result);

result = each([],"sa");
console.log(result);

result = each({},2);
console.log(result);

result = each(323,cb);
console.log(result);