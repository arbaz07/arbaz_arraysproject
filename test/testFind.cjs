const elements = require("../data.cjs");
const find = require("../find.cjs")

function callBackFunc(element, searchFor){
    if(element == searchFor){
        return true;
    }else{
        return false;
    }
}

console.log(find(elements,callBackFunc,55));

console.log(find([1,4],callBackFunc,4));
console.log(find());
console.log(find(callBackFunc,4));
console.log(find(4));
console.log(find("sadasd",3,callBackFunc));
console.log(find(1,3,3));
console.log(find(callBackFunc,{sa:"Asd"},"sda"));
console.log(find(12,"",'sad'));
console.log(find(callBackFunc,34,callBackFunc));
console.log(find([]));
console.log(find("",",",true));
console.log(find(false,undefined,NaN));
console.log(find(null,NaN,true));


console.log(find(elements,{},callBackFunc));
