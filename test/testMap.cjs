const map = require("../map.cjs");
const element = require("../data.cjs");



function getSquared(element){
    return element*2;
}

console.log(map(element,getSquared));
console.log(map({},getSquared));
console.log(map(1));

console.log(map([],getSquared));
console.log(map(5454,getSquared));

console.log(map(element,"ds"));
console.log(map(element));
console.log(map(getSquared));