const elements = require("../data.cjs");
const filter = require("../filter.cjs");



function isEven(num){
    if(num%2==0){
        return true;
    }else{
        return false;
    }
}


function isOdd(num){
    if(num%2!==0){
        return true;
    }else{
        return false;
    }
}

console.log(filter(elements,isEven));
console.log(filter(elements,isOdd));

console.log(filter(isOdd,isEven));
console.log(filter([],{}));
console.log(filter(1,true));
console.log(filter(undefined,isOdd));
console.log(filter('elements',12));
console.log(filter(null,true));
console.log(filter(23,isEven));
console.log(filter(elements,isOdd));
console.log(filter({"sda":[1,2,3,4,3],12:"sda"},isEven));
console.log(filter([],isOdd));