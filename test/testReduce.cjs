const data = require("../data.cjs");
const reduce = require("../reduce.cjs");


function callBackFun(startingVal, num){
    return startingVal+num;
}

console.log(reduce());
console.log(reduce(data,callBackFun,5));

console.log(reduce(data,callBackFun,"sda"));
console.log(reduce([],callBackFun,{}));
console.log(reduce(data,"asfs",3));