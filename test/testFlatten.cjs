// const elements = require("../data.cjs");
const flatten = require("../flatten.cjs");

const element = [1, [2], [[3]], [[[4]]]];



console.log(flatten(element));
console.log(flatten(324));
console.log(flatten([1,23,4]));
console.log(flatten({1:"DFA"}));
console.log(flatten(null));
console.log(flatten(undefined));
console.log(flatten(NaN));
console.log(flatten("sda"));
console.log(flatten());
console.log(flatten(true));
console.log(flatten(false));
console.log(flatten(flatten));
console.log(flatten(44.4));
console.log(flatten(0));
console.log(flatten());
console.log(flatten(element));
console.log(flatten("fsdfdsfs",null));