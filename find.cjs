const { isNumber } = require("underscore");

function find(elements, callBack, searchElement){
    if(elements instanceof Array && typeof(callBack)=='function' && isNumber(searchElement)){
        for(index=0; index<elements.length; index++){
            if(callBack(elements[index],searchElement)){
                return elements[index];
            }
        }
        return undefined;
    }
    return {};
}

module.exports = find;


