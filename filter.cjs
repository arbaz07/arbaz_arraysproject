
function filter(elements, callBack){
    if(elements instanceof Array && typeof(callBack)=='function'){
        let filteredArray = [];
        for(let index=0; index<elements.length; index++){

            let result = callBack(elements[index],index, elements);
            if(true===result){
                //console.log(index);
                filteredArray.push(elements[index]);
            }
        }
        return filteredArray;
    }
    return [];
}

// const ArrayOfNumbers=["sdadsads","SDasdasdsdsa","sdadsd","GRTETRW"];
// function lengthOf(element){
//     //console.log(element);
//     if(element.length<7){
//         return true;
//     }else{
//         return false;
//     }
// }

// const result = filter(ArrayOfNumbers,lengthOf);
// const result2 = filter();
// console.log(result);

module.exports = filter;